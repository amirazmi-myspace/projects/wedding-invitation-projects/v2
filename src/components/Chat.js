import { useEffect, useState } from "react"
import { DATABASE, FIREBASE } from "../configs/firebase"

export const Chat = ({ chatboxStyle = 'flex flex-col h-5/6 md:h-4/5 bg-gray-50 md:w-3/5 text-gray-700 p-5 rounded-2xl' }) => {


  const [guestName, setGuestName] = useState('')
  const [message, setMessage] = useState('')

  const [load, setLoad] = useState(1)

  const [guestMessage, setGuestMessage] = useState([])

  useEffect(() => {
    fetchMessage()
  }, [])

  useEffect(() => {
    scrollChatBoxToBottom()
  }, [guestMessage])


  const scrollChatBoxToBottom = () => {
    const messageEnd = document.querySelector('#messageEnd')
    const containerChatBox = document.querySelector('#chatbox')
    const bounding = containerChatBox.getBoundingClientRect()
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= window.innerWidth && bounding.bottom <= window.innerHeight) {
      console.log('masuk dah')
      messageEnd.scrollIntoView({ behavior: 'smooth' })
    } else {
      console.log('xlagi')
    }
  }

  const fetchMessage = async () => {
    await DATABASE.ref('/message').on('value', (snapshot) => {

      if (snapshot.exists()) {
        const DB_MESSAGE_ARRAY = []
        for (const key in snapshot.val()) {
          DB_MESSAGE_ARRAY.push(snapshot.val()[key])
        }
        setGuestMessage(DB_MESSAGE_ARRAY)
        return setLoad(2)
      }
      return setLoad(3)
    })
  }

  const submitMessage = async (e) => {
    e.preventDefault()
    if (guestName === '' || message === '') return
    const guestMessageInput = {
      name: guestName,
      message: message,
      timestamp: FIREBASE.database.ServerValue.TIMESTAMP
    }
    await DATABASE.ref('/message').push(guestMessageInput)
    await fetchMessage()
    setGuestName('')
    setMessage('')
  }


  const _renderChatContent = () => {

    const sorted = guestMessage.sort((a, b) => a.timestamp < b.timestamp)
    if (load === 1) return null
    if (load === 2) {
      return sorted.map(chat => {
        let name
        if (chat.name.includes('5391')) name = chat.name.replace('5391', '')
        if (!chat.name.includes('5391')) name = chat.name

        return (
          <li className='flex flex-row bg-gray-200 my-3 mx-5 p-3 w-4/5 rounded-2xl' style={{ marginLeft: (chat.name.includes('5391')) && 'auto', backgroundColor: (chat.name.includes('5391')) && '#3EB7B2', color: (chat.name.includes('5391')) && '#fff' }}>
            <div className='flex flex-col w-full'>
              <div className='text-sm tracking-widest mb-2 font-bold'>{name.toUpperCase()}</div>
              <div>{chat.message.toString()}</div>
              <div className='ml-auto mt-2 italic text-xs'>{new Date(chat.timestamp).toLocaleDateString('en-MY')} - {new Date(chat.timestamp).toLocaleTimeString()}</div>
            </div>
          </li>
        )
      })
    }
    if (load === 3) {
      return (
        <div className='text-gray-500 text-center mt-10'>
          <p>Nampaknya belum ada yang berbual dengan pengantin...</p>
          <p>Jadilah yang pertama berbual dengan pengantin.</p>
        </div>
      )
    }
  }
  const _renderChat = () => {
    return (
      <div id='chatbox' className={chatboxStyle}>
        <ul className='overflow-y-scroll'>
          {_renderChatContent()}
          <div style={{ float: "left", clear: "both" }} id='messageEnd'></div>
        </ul>
        <form onSubmit={submitMessage} className='flex flex-row mt-auto w-full'>
          <div className='flex flex-col w-full pt-2'>
            <input type="text" name='name' value={guestName} placeholder='Enter your name' className='rounded-2xl w-full border border-solid border-gray-500 p-3 mb-3' onChange={(e) => setGuestName(e.target.value)} />
            <textarea name="body" value={message} className='rounded-2xl w-full border border-solid border-gray-500 h-50 p-3' onChange={(e) => setMessage(e.target.value)}></textarea>
          </div>
          <button type='submit' className='rounded-2xl ml-2 mt-2 px-5 py-3 bg-primary-dark text-gray-50'>Send</button>
        </form>
      </div>
    )
  }

  return _renderChat()

}