import GoogleMapReact from 'google-map-react'

const AnyReactComponent = ({ text }) => (
  <div>
    <i class="fas fa-map-marker-alt fa-4x" style={{ color: 'red' }}></i>
    <p>{text}</p>
  </div>
)

export const Map = ({ mapLocation }) => {

  const windowHeight = window.innerHeight
  return (
    // Important! Always set the container height explicitly
    <div className='shadow-md' style={{ height: windowHeight <= 750 ? '270px' : '400px', width: '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyA-VPHWAF6RUTxQ4rXcL67mxMh1yLNHI0k' }}
        defaultCenter={mapLocation}
        defaultZoom={18}
      >
        <AnyReactComponent
          lat={mapLocation.lat}
          lng={mapLocation.lng}
          text={mapLocation.marker}
        />
      </GoogleMapReact>
    </div>
  );
}
