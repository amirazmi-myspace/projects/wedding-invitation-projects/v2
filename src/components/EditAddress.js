import { useEffect, useState } from 'react'
import { DATABASE } from '../configs/firebase'

export const EditAddress = ({ toggle, setToggle, parentRefresh, locationInfo }) => {

  const [location, setLocation] = useState({})

  useEffect(() => {
    setLocation(locationInfo)
  }, [locationInfo])

  console.log(location)

  const submitLocation = async () => {
    const who = 'bride'
    await DATABASE.ref(`wedding/${who}/location`).update(location)
    setToggle(false)
    parentRefresh()
  }


  const _renderForm = () => {
    return (
      <form className='flex flex-col justify-center items-center w-full'>
        <div className='flex flex-col justify-center items-center w-full mb-3'>
          <label>Alamat Walimatulurus</label>
          <input onChange={(e) => setLocation({ ...location, address: e.target.value })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={location && location.address} placeholder='Isi alamat majlis' />
          <input onChange={(e) => setLocation({ ...location, lat: e.target.value })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={location && location.lat} placeholder='Isi latitude (berdasarkan googlemaps)' />
          <input onChange={(e) => setLocation({ ...location, lng: e.target.value })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={location && location.lng} placeholder='Isi longitude (berdasarkan googlemaps)' />
          <input onChange={(e) => setLocation({ ...location, marker: e.target.value })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={location && location.marker} placeholder='Isi label marker)' />
          <input onChange={(e) => setLocation({ ...location, link: e.target.value })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={location && location.link} placeholder='Isi link share google maps' />
        </div>
      </form>
    )
  }

  const _renderModalContent = () => {
    return (
      <>
        <div className='text-sm flex flex-col'>
          <p className='font-bold uppercase text-lg text-center mb-5'>Tukar Info Perkahwinan</p>
          <div className='flex flex-row w-full justify-center items-center'>
            {_renderForm()}
          </div>
        </div>
        <div className='flex flex-row mt-3'>
          <button onClick={() => setToggle(false)} className='text-sm text-red-500 uppercase'>Batal</button>
          <button onClick={submitLocation} className='text-sm ml-auto uppercase py-2 px-5 bg-primary-dark text-gray-50 rounded-full'>Ubah</button>
        </div>
      </>
    )
  }

  const _renderModal = () => {
    if (!toggle) return null
    if (toggle) {
      return (
        <div className="fixed inset-0 z-50 overflow-auto bg-smoke-light flex">
          <div className="p-6 bg-white w-full max-w-xs md:max-w-md m-auto flex-col flex rounded-lg">
            {_renderModalContent()}
          </div>
        </div>
      )
    }
  }

  return _renderModal()
}
