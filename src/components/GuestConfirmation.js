import React, { useEffect, useState } from 'react'
import { DATABASE } from '../configs/firebase'

export const GuestConfirmation = ({ toggle, setToggle }) => {

  const [guestName, setGuestName] = useState('')
  const [guestContact, setGuestContact] = useState('')
  const [guestNumberOfMember, setGuestNumberOfMember] = useState()
  const [confirmed, setConfirmed] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')

  const submitGuestData = async (e) => {
    e.preventDefault()
    const guestInputData = {
      name: guestName,
      contact: guestContact,
      numberOfPeople: guestNumberOfMember
    }

    if (guestName === '') return setErrorMsg('Ruang nama tidak boleh kosong!')
    if (guestContact === '') return setErrorMsg('Ruang telefon tidak boleh kosong!')
    if (guestNumberOfMember === '') return setErrorMsg('Ruang jumlah yang hadir tidak boleh kosong!')

    await DATABASE.ref('/guest_confirm').push(guestInputData)
    setConfirmed(true)
    setGuestName('')
    setGuestContact('')
    setGuestNumberOfMember('')
    setTimeout(() => {
      setToggle(false)
    }, 3000)
  }


  const _renderModal = () => {
    if (!toggle) return null
    if (toggle && !confirmed) {
      return (
        <div className="fixed inset-0 z-50 overflow-auto bg-smoke-light flex">
          <div className="p-6 bg-white w-full max-w-xs md:max-w-md m-auto flex-col flex rounded-lg">
            <form onSubmit={submitGuestData}>
              <div className='text-sm'>
                <p className='font-bold uppercase text-lg text-center mb-5'>Sahkan kehadiran</p>
                {errorMsg !== '' && <p className='border border-solid border-red-500 text-red-500 text-center p-3 w-full uppercase'>{errorMsg}</p>}
                <div className='flex flex-col items-center w-full my-3'>
                  <label>Nama: </label>
                  <input className='border border-gray-500 p-1 w-full text-center' type="text" name="name" value={guestName} onChange={(e) => { setGuestName(e.target.value); setErrorMsg('') }} placeholder='nama anda...' />
                </div>
                <div className='flex flex-col items-center w-full my-3'>
                  <label>No Telefon: </label>
                  <input className='border border-gray-500 p-1 w-full text-center' type="text" name="name" value={guestContact} onChange={(e) => { setGuestContact(e.target.value); setErrorMsg('') }} placeholder='no telefon anda...' />
                </div>
                <div className='flex flex-col items-center w-full my-3'>
                  <label>Jumlah Yang Hadir: </label>
                  <input className='border border-gray-500 p-1 w-full text-center' type="number" name="name" value={guestNumberOfMember} onChange={(e) => { setGuestNumberOfMember(e.target.value); setErrorMsg('') }} placeholder='contoh: 15' />
                </div>
              </div>
              <span className="flex justify-between w-full mt-5">
                <button className='p-2 text-sm text-red-500 uppercase' onClick={() => setToggle(false)}>Batal</button>
                <button className='py-2 px-5 text-sm bg-blue-500 text-gray-50 uppercase'>Sahkan</button>
              </span>
            </form>
          </div>
        </div>
      )
    }
    if (toggle && confirmed) {
      return (
        <div className="fixed inset-0 z-50 overflow-auto bg-smoke-light flex">
          <div className="p-6 bg-white w-full max-w-xs md:max-w-md m-auto flex-col flex rounded-lg">

            <div className='text-sm'>
              <p className='font-bold uppercase text-lg text-center mb-5'>Sahkan kehadiran</p>
              <div className='flex flex-col items-center w-full my-3'>
                <p className='text-center text-lg'>Terima kasih kerana sahkan kehadiran anda. Jumpa anda di majlis nanti. Terima kasih.</p>
              </div>
            </div>
            <span className="flex justify-start w-full mt-5">
              <button className='p-2 text-sm text-red-500 uppercase' onClick={() => setToggle(false)}>Batal</button>
            </span>
          </div>
        </div>
      )
    }
  }

  return _renderModal()
}
