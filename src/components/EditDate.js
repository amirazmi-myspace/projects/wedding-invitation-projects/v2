import React, { useState } from 'react'
import DatePicker from "react-datepicker";
import { DATABASE } from '../configs/firebase'
import "react-datepicker/dist/react-datepicker.css";

export const EditDate = ({ toggle, setToggle, parentRefresh }) => {

  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())

  const submitDate = async () => {
    const who = 'bride'
    const start = new Date(startDate).getTime()
    const end = new Date(endDate).getTime()

    await DATABASE.ref(`/wedding/${who}/timestamp_start`).set(start)
    await DATABASE.ref(`/wedding/${who}/timestamp_end`).set(end)
    parentRefresh()
    setToggle(false)
  }

  const _renderModal = () => {
    if (!toggle) return null
    if (toggle) {
      return (
        <div className="fixed inset-0 z-50 overflow-auto bg-smoke-light flex">
          <div className="p-6 bg-white w-full max-w-xs md:max-w-md m-auto flex-col flex rounded-lg">
            <div className='text-sm flex flex-col'>
              <p className='font-bold uppercase text-lg text-center mb-5'>Tukar Tarikh dan Masa</p>
              <div className='flex flex-row w-full justify-center items-center'>
                <div className='flex-1 w-full'>
                  <p className='pb-2'>Mula</p>
                  <DatePicker dateFormat="Pp" selected={startDate} onChange={(date) => { setStartDate(date) }} showTimeSelect className='border rounded-full border-gray-500 border-solid text-center p-1 text-md cursor-pointer' />
                </div>
                <div className='flex-1 w-full'>
                  <p className='pb-2'>Habis</p>
                  <DatePicker dateFormat="Pp" selected={endDate} onChange={(date) => { setEndDate(date) }} showTimeSelect className='border rounded-full border-gray-500 border-solid text-center p-1 text-md cursor-pointer' />
                </div>
              </div>
            </div>
            <div className='flex flex-row mt-3'>
              <button onClick={() => setToggle(false)} className='text-sm text-red-500 uppercase'>Batal</button>
              <button onClick={submitDate} className='text-sm ml-auto uppercase py-2 px-5 bg-primary-dark text-gray-50 rounded-full'>Ubah</button>
            </div>
          </div>
        </div>
      )
    }
  }

  return _renderModal()
}
