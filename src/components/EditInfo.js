import { useEffect, useState } from 'react'
import { DATABASE } from '../configs/firebase'

export const EditInfo = ({ toggle, setToggle, parentRefresh, weddingInfo }) => {

  const [info, setInfo] = useState({})

  useEffect(() => {
    setInfo(weddingInfo)
  }, [weddingInfo])

  const submitInfo = async () => {
    const who = 'bride'
    await DATABASE.ref(`wedding/${who}/`).update(info)
    setToggle(false)
    parentRefresh()
  }


  const _renderForm = () => {
    return (
      <form className='flex flex-col justify-center items-center w-full'>
        <div className='flex flex-col justify-center items-center w-full mb-3'>
          <label>Pengantin Perempuan</label>
          <input onChange={(e) => setInfo({ ...info, bride: { ...info.bride, name: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.bride ? info.bride.name : ''} placeholder='Isi nama pengantin perempuan' />
          <input onChange={(e) => setInfo({ ...info, bride: { ...info.bride, phone: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.bride ? info.bride.phone : ''} placeholder='Isi no fon pengantin perempuan' />
          <input onChange={(e) => setInfo({ ...info, bride: { ...info.bride, whatsapp: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.bride ? info.bride.whatsapp : ''} placeholder='Isi no whatsapp pengantin perempuan' />
        </div>
        <div className='flex flex-col justify-center items-center w-full mb-3'>
          <label>Pengantin Lelaki</label>
          <input onChange={(e) => setInfo({ ...info, groom: { ...info.groom, name: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.groom ? info.groom.name : ''} placeholder='Isi nama pengantin lelaki' />
          <input onChange={(e) => setInfo({ ...info, groom: { ...info.groom, phone: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.groom ? info.groom.phone : ''} placeholder='Isi no fon pengantin lelaki' />
          <input onChange={(e) => setInfo({ ...info, groom: { ...info.groom, whatsapp: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.groom ? info.groom.whatsapp : ''} placeholder='Isi no whatsapp pengantin lelaki' />
        </div>
        <div className='flex flex-col justify-center items-center w-full mb-3'>
          <label>Bapa Pengantin</label>
          <input onChange={(e) => setInfo({ ...info, father: { ...info.father, name: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.father ? info.father.name : ''} placeholder='Isi nama bapa pengantin' />
          <input onChange={(e) => setInfo({ ...info, father: { ...info.father, phone: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.father ? info.father.phone : ''} placeholder='Isi no fon bapa pengantin' />
          <input onChange={(e) => setInfo({ ...info, father: { ...info.father, whatsapp: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.father ? info.father.whatsapp : ''} placeholder='Isi no whatsapp bapa pengantin' />
        </div>
        <div className='flex flex-col justify-center items-center w-full mb-3'>
          <label>Ibu Pengantin</label>
          <input onChange={(e) => setInfo({ ...info, mother: { ...info.mother, name: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.mother ? info.mother.name : ''} placeholder='Isi nama ibu pengantin' />
          <input onChange={(e) => setInfo({ ...info, mother: { ...info.mother, phone: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.mother ? info.mother.phone : ''} placeholder='Isi no fon ibu pengantin' />
          <input onChange={(e) => setInfo({ ...info, mother: { ...info.mother, whatsapp: e.target.value } })} className='p-1 m-2 text-center text-lg text-gray-700 border border-gray-500 w-full rounded-full' type="text" value={info.mother ? info.mother.whatsapp : ''} placeholder='Isi no whatsapp ibu pengantin' />
        </div>
      </form>
    )
  }

  const _renderModalContent = () => {
    return (
      <>
        <div className='text-sm flex flex-col'>
          <p className='font-bold uppercase text-lg text-center mb-5'>Tukar Info Perkahwinan</p>
          <div className='flex flex-row w-full justify-center items-center'>
            {_renderForm()}
          </div>
        </div>
        <div className='flex flex-row mt-3'>
          <button onClick={() => setToggle(false)} className='text-sm text-red-500 uppercase'>Batal</button>
          <button onClick={submitInfo} className='text-sm ml-auto uppercase py-2 px-5 bg-primary-dark text-gray-50 rounded-full'>Ubah</button>
        </div>
      </>
    )
  }

  const _renderModal = () => {
    if (!toggle) return null
    if (toggle) {
      return (
        <div className="fixed inset-0 z-50 overflow-auto bg-smoke-light flex">
          <div className="p-6 bg-white w-full max-w-xs md:max-w-md m-auto flex-col flex rounded-lg">
            {_renderModalContent()}
          </div>
        </div>
      )
    }
  }

  return _renderModal()
}
