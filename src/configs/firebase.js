import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

firebase.initializeApp({
  apiKey: "AIzaSyA-VPHWAF6RUTxQ4rXcL67mxMh1yLNHI0k",
  authDomain: "weds-inv.firebaseapp.com",
  databaseURL: "https://weds-inv.firebaseio.com",
  projectId: "weds-inv",
  storageBucket: "weds-inv.appspot.com",
  messagingSenderId: "791313451915",
  appId: "1:791313451915:web:8f58aede4785d511773d8e"
})

export const FIREBASE = firebase
export const DATABASE = firebase.database()
export const AUTH = firebase.auth()