export const DateFormat = (startDateInput, endDateInput) => {
  const dayList = ['Ahad', 'Isnin', 'Selasa', 'Rabu', 'Khamis', 'Jumaat', 'Sabtu']
  const monthList = ['Januari', 'Februari', 'Mac', 'April', 'Mei', 'Jun', 'Julai', 'Ogos', 'September', 'Oktober', 'November', 'Disember']
  const hourList = ['12', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']

  // Date
  const day = dayList[new Date(startDateInput).getDay()].toString()
  const date = new Date(startDateInput).getDate().toString()
  const month = monthList[new Date(startDateInput).getMonth()].toString()
  const year = new Date(startDateInput).getFullYear().toString()

  // Time - start
  let startHours = new Date(startDateInput).getHours()
  let startHoursTwelve = hourList[startHours].toString()
  let startMinutes = new Date(startDateInput).getMinutes().toString()
  let pmamStart, startTimeStr

  if (startHoursTwelve.split('').length < 2) startHoursTwelve = `0${hourList[startHours].toString()}`
  if (startMinutes.split('').length < 2) startMinutes = `0${new Date(startDateInput).getMinutes().toString()}`

  if (startHours < 13) pmamStart = 'a.m.'
  if (startHours > 12) pmamStart = 'p.m.'
  startTimeStr = `${startHoursTwelve.toString()} : ${startMinutes} ${pmamStart}`

  let pmamEnd, endHours, endHoursTwelve, endMinutes, endTimeStr
  if (endDateInput) {
    // Time - end
    endHours = new Date(endDateInput).getHours()
    endHoursTwelve = hourList[endHours].toString()
    endMinutes = new Date(endDateInput).getMinutes().toString()

    if (endHoursTwelve.split('').length < 2) endHoursTwelve = `0${hourList[endHours].toString()}`
    if (endMinutes.split('').length < 2) endMinutes = `0${new Date(endDateInput).getMinutes().toString()}`

    if (endHours < 13) pmamEnd = 'a.m.'
    if (endHours > 12) pmamEnd = 'p.m.'

    endTimeStr = `${endHoursTwelve.toString()} : ${endMinutes} ${pmamEnd}`
  }

  const timeStr = endDateInput ? `${startTimeStr} - ${endTimeStr}` : `${startTimeStr}`

  return { day, dateStr: `${date} ${month} ${year}`, timeStr }
}