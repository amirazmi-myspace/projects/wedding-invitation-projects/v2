import { render } from 'react-dom'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import './assets/main.css'
import { Admin } from './screens/Admin'
import { Home } from './screens/Home'

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/admin' exact component={Admin} />
        <Redirect from='*' to='/' />
      </Switch>
    </Router>
  )
}

render(<App />, document.querySelector('#root'))