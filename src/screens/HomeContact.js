import { Chat } from "../components/Chat"

export const HomeContact = ({ weddingInfo }) => {

  const _renderContact = () => {
    const nameArray = []
    for (const key in weddingInfo) {
      if (!key.includes('timestamp') && !key.includes('location')) {
        nameArray.push(weddingInfo[key])
      }
    }
    return (
      <div className='flex flex-col md:flex-1 items-center justify-center h-screen md:h-full'>
        <div className='flex mb-5'>
          <p className='text-xl md:text-4xl text-center px-5'>Sebarang pertanyaan dan maklumat lanjut boleh hubungi kami</p>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-2">
          {nameArray.sort((a, b) => b.order - a.order).map((name) => (
            <div className='text-lg md:text-2xl flex flex-col justify-center text-center my-2 md:my-7 mx-5'>
              <p className='text-4xl'>{name.name}</p>
              <p className='capitalize text-lg my-1'>({name.label})</p>
              <p className='pt-1 md:pt-3'><i class="fas fa-phone-square-alt"></i> {name.phone}</p>
              <p className='pt-1 md:pt-3'><i class="fab fa-whatsapp-square"></i> {name.whatsapp}</p>
            </div>
          ))}
        </div>
      </div>
    )
  }

  return (
    <div className='px-2 md:px-0 flex flex-col md:flex-row md:h-screen w-screen font-main bg-gray-500 text-gray-100 justify-center'>
      {_renderContact()}
      <div className='flex flex-col md:flex-1 items-center justify-center h-screen md:h-full'>
        <p className='text-center text-lg py-3'>Jom berbual bersama pengantin</p>
        <Chat />
      </div>
    </div >
  )
}
