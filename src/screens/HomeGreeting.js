import { useState } from "react"
import { GuestConfirmation } from "../components/GuestConfirmation"
import { Map } from "../components/Map"
import { DateFormat } from "../utils/DateFormat"

export const HomeGreeting = ({ weddingInfo }) => {
  const [toggle, setToggle] = useState(false)

  const _renderGreet = () => {
    return (
      <div className='container-greet'>
        <p className='greet-small'>Assalamualaikum Warahmatullahi Wabarakatuh & Salam Sejahtera</p>
        <p className='parent-name'>{weddingInfo.father.name}</p>
        <p className='parent-name-add'>&</p>
        <p className='parent-name'>{weddingInfo.mother.name}</p>
        <p className='invitation-text'>dengan setulus hati <span className='invitation-text-border'>menjemput anda</span></p>
        <p className='greet-small'>sekeluarga ke majlis perkahwinan puteri kami</p>
        <p className='bridegroom bridegroom-name'>{weddingInfo.bride.name}</p>
        <p className='bridegroom bridegroom-and'>- & -</p>
        <p className='bridegroom bridegroom-name'>{weddingInfo.groom.name}</p>
      </div>
    )
  }

  const _renderDate = () => {
    const fullDate = DateFormat(weddingInfo.timestamp_start, weddingInfo.timestamp_end)
    return (
      <>
        <p className='greet-date'>{fullDate.dateStr} | {fullDate.day}</p>
        <p className='greet-hijri'>08 Rejab 1441H</p>
        <p className='greet-date'>{fullDate.timeStr}</p>
      </>
    )
  }

  const _renderMap = () => {

    const mapLocation = {
      marker: weddingInfo.location.marker,
      lat: parseFloat(weddingInfo.location.lat),
      lng: parseFloat(weddingInfo.location.lng)
    }

    return (
      <>
        <p id='map' className='location-title'>Lokasi</p>
        <address className='location-address'>{weddingInfo.location.address}</address>
        <div className='w-full mt-3'>
          <Map mapLocation={mapLocation} />
        </div>
        <div className='flex justify-center mt-5 w-full'>
          <a href={weddingInfo.location.link} target="_blank" className='main-btn main-btn-light'>Maps</a>
          <a href={`https://waze.com/ul?ll=${weddingInfo.location.lat},${weddingInfo.location.lng}&navigate=yes`} target="_blank" className='main-btn main-btn-light'>Waze</a>
        </div>
      </>
    )
  }

  return (
    <>
      <div className='home-greet-container'>
        {_renderGreet()}
        <div className='container-greet'>
          {_renderDate()}
          <button onClick={() => setToggle(!toggle)} className='main-btn main-btn-light mt-5'>Sahkan Kehadiran Anda</button>
          {_renderMap()}
        </div>
        <div className='note-box'>
          <p className='text-sm md:text-md'>Semoga dengan kehadiran dan doa restu para tetamu sekalian akan menyerikan lagi majlis kami dan diberkati Allah SWT</p>
          <p className='md:text-sm'>Sila sahkan kedatangan tuan/puan sekeluarga dengan tekan butang di bawah</p>
          <button onClick={() => setToggle(!toggle)} className='main-btn main-btn-light mt-5'>Sahkan Kehadiran Anda</button>
        </div>
      </div>
      <GuestConfirmation toggle={toggle} setToggle={setToggle} />
    </>
  )
}
