import { Switch } from "react-router-dom"
import { AdminDashboard } from "./AdminDashboard"

export const Admin = () => {

  return (
    <div className='flex w-screen h-screen bg-gray-100 font-main'>
      <AdminDashboard />
    </div>
  )
}
