import { useEffect, useState } from 'react'
import { GuestConfirmation } from '../components/GuestConfirmation'
import { DateFormat } from '../utils/DateFormat'

export const HomeHero = ({ weddingInfo }) => {
  const [toggle, setToggle] = useState(false)
  const [countdown, setCountdown] = useState([])

  useEffect(() => {
    countDown()
  }, [])

  const countDown = () => {
    setInterval(() => {

      let days = {}
      let hours = {}
      let minutes = {}
      let seconds = {}

      const currentDateTime = new Date().getTime()
      const engagementDate = new Date(weddingInfo.timestamp_start).getTime()
      const timeleft = engagementDate - currentDateTime

      const daysLeft = Math.floor(timeleft / (1000 * 60 * 60 * 24)).toString()
      const hoursLeft = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)).toString()
      const minutesLeft = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60)).toString()
      const secondsLeft = Math.floor((timeleft % (1000 * 60)) / 1000).toString()


      days.count = daysLeft
      days.order = 0
      days.label = 'Hari'

      hours.count = hoursLeft
      hours.order = 1
      hours.label = 'Jam'

      minutes.count = minutesLeft
      minutes.order = 2
      minutes.label = 'Minit'

      seconds.count = secondsLeft
      seconds.order = 3
      seconds.label = 'Saat'


      if (secondsLeft.length < 2) seconds.count = `0${secondsLeft}`
      if (minutesLeft.length < 2) minutes.count = `0${minutesLeft}`
      if (hoursLeft.length < 2) hours.count = `0${hoursLeft}`
      if (daysLeft.length === 2) days.count = `0${daysLeft}`
      if (daysLeft.length === 1) days.count = `00${daysLeft}`

      const countdownArray = [days, hours, minutes, seconds]
      setCountdown(countdownArray)

    }, 1000)
  }

  const _renderCountdown = () => {
    return (
      <div className='flex text-gray-100 mt-4 md:mt-8'>
        {countdown.sort((a, b) => a.order - b.order).map((cd) => {
          return (
            <div key={cd.order} className='countdown-container lg:mx-1 xl:mx-2'>
              <div className='countdown-label'>{cd.label}</div>
              <div className='flex'>
                {cd.count.split('').map((num, index) => <div key={index} className='countdown-number'>{num}</div>)}
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  const _renderDate = () => {

    const datetime = DateFormat(weddingInfo.timestamp_start)
    return (
      <div className='container bg-gray-500 '>
        <div className='container-inner mb-28 md:mb-0'>
          <small className='text-gray-300 small-title'>p a d a</small>
          <p className='date'>{datetime.dateStr} | {datetime.day}</p>
          {_renderCountdown()}
        </div>
        <div className='visible absolute bottom-16 md:bottom-48 md:self-start mt-10'>
          <a href='#map' className='main-btn main-btn-dark'>Tempat</a>
          <a onClick={() => setToggle(!toggle)} className='md:invisible main-btn main-btn-dark'>Akan Hadir</a>
        </div>
      </div>
    )
  }

  const _renderBridesName = () => {
    return (
      <div className='container '>
        <div className='container-inner mt-7 md:mt-0'>
          <small className='text-gray-500 small-title'>W a l i m a t u l U r u s</small>
          <p className='bridegroom bridegroom-name'>{weddingInfo.bride.name}</p>
          <p className='bridegroom bridegroom-and'>- & -</p>
          <p className='bridegroom bridegroom-name'>{weddingInfo.groom.name}</p>
        </div>
        <div className='invisible md:visible md:absolute md:bottom-48 md:self-end md:mt-10'>
          <a onClick={() => setToggle(!toggle)} className='main-btn main-btn-light'>Akan Hadir</a>
        </div>
      </div>
    )
  }

  return (
    <div className='flex flex-col md:flex-row h-screen justify-center items-center font-main bg-gray-100 '>
      {_renderBridesName()}
      {_renderDate()}
      <GuestConfirmation toggle={toggle} setToggle={setToggle} />
    </div>
  )
}