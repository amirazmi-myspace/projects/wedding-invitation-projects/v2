import { useEffect, useState } from "react"
import { Chat } from "../components/Chat"
import { EditDate } from "../components/EditDate"
import { EditInfo } from "../components/EditInfo"
import { EditAddress } from "../components/EditAddress"
import { DATABASE } from "../configs/firebase"
import { DateFormat } from "../utils/DateFormat"

export const AdminDashboard = () => {

  const [toggleEditDate, setToggleEditDate] = useState(false)
  const [toggleEditInfo, setToggleEditInfo] = useState(false)
  const [toggleEditAddress, setToggleEditAddress] = useState(false)
  const [weddingInfo, setWeddingInfo] = useState({})
  const [guestConfirm, setGuestConfirm] = useState([])
  const [load, setLoad] = useState(1)

  useEffect(() => {
    fetchGuest()
    fetchWedding()
  }, [])

  const fetchWedding = async () => {
    const who = 'bride'
    const DB_SNAP = await DATABASE.ref(`/wedding/${who}`).once('value')
    await DB_SNAP.exists() && setWeddingInfo(DB_SNAP.val())
    setLoad(2)
  }

  const fetchGuest = async () => {
    await DATABASE.ref('/guest_confirm').on('value', (snapshot) => {
      if (snapshot.exists()) {
        let guestArray = []
        for (const key in snapshot.val()) {
          guestArray.push(snapshot.val()[key])
        }
        setGuestConfirm(guestArray)
      }
    })
  }

  const _renderDate = () => {

    let weddingDate
    if (load === 1) weddingDate = 'loading'
    if (load === 2) weddingDate = DateFormat(weddingInfo.timestamp_start, weddingInfo.timestamp_end)


    return (
      <div className='p-2 bg-gray-50 row-span-1 col-span-1 border border-gray-300 shadow-md rounded-3xl flex flex-col'>
        <button onClick={() => setToggleEditDate(true)} className='text-md ml-auto mr-5 text-primary-dark'><i class="fas fa-pencil-alt"></i></button>
        <p className='py-2 text-4xl text-center'>{load === 2 && weddingDate.dateStr} | <span className='text-2xl'>{load === 2 && weddingDate.timeStr}</span></p>
        <p className='py-2 tracking-widest text-md text-center text-gray-500'>Tarikh Perkahwinan</p>
      </div>
    )
  }

  const _renderGuestNumber = () => {


    const guestNumberArray = guestConfirm.map(guest => parseInt(guest.numberOfPeople))
    const totalGuest = guestNumberArray.reduce((a, b) => a + b)

    return (
      <div className='p-2 bg-gray-50 row-span-1 col-span-1 border border-gray-300 shadow-md rounded-3xl flex flex-col'>
        <button className='invisible text-md ml-auto mr-5 text-primary-dark'><i class="fas fa-pencil-alt"></i></button>
        <p className='py-2 text-4xl text-center'>{totalGuest} Orang</p>
        <p className='py-2 tracking-widest text-md text-center text-gray-500'>Jumlah Hadir</p>
      </div>
    )
  }

  const _renderAddress = () => {



    return (
      <div className='p-2 bg-gray-50 row-span-1 col-span-1 border border-gray-300 shadow-md rounded-3xl flex flex-col'>
        <button onClick={() => setToggleEditAddress(true)} className='text-md ml-auto mr-5 text-primary-dark'><i class="fas fa-pencil-alt"></i></button>
        <p className='py-1 text-lg text-center'>{weddingInfo.location.address}</p>

        <div className='flex flex-row w-full justify-around items-center'>
          <p className='text-sm text-center'>Lat: {weddingInfo.location.lat}</p>
          <p className='text-sm text-center'>Lng: {weddingInfo.location.lng}</p>
        </div>

        <p className='py-2 tracking-widest text-md text-center text-gray-500'>Alamat Walimatulurus</p>
      </div>
    )
  }


  const _renderWeddingDetail = () => {
    const infoArray = []
    for (const key in weddingInfo) {
      if (!key.includes('timestamp') && !key.includes('location')) infoArray.push(weddingInfo[key])
    }

    return (
      <div className='p-2 bg-gray-50 col-span-1 row-span-4 border border-gray-300 shadow-md rounded-3xl flex flex-col items-center'>
        <button onClick={() => setToggleEditInfo(true)} className='text-md ml-auto mr-5 text-primary-dark'><i class="fas fa-pencil-alt"></i></button>
        <p className='my-3 text-lg tracking-widest'>Maklumat Kahwin</p>
        {infoArray.sort((a, b) => a.order - b.order).map((info) => (
          <div className='my-3 flex flex-col items-center'>
            <p className='text-sm text-gray-500 capitalize'>{info.label}</p>
            <p className='text-4xl py-2'>{info.name}</p>
            <p className='tracking-widest'>Telefon No: {info.phone}</p>
            <p className='tracking-widest'>Whatsapp: {info.whatsapp}</p>
          </div>
        )
        )}
      </div>
    )
  }

  if (load === 1) return (
    <div className='flex justify-center items-center w-screen'>
      <p className='text-8xl text-center'>Loading...</p>
    </div>
  )
  if (load === 2) return (
    <>
      <div className='grid grid-rows-5 grid-cols-3 gap-4 w-screen m-10 uppercase'>
        {_renderDate()}
        {_renderGuestNumber()}
        {_renderAddress()}
        <div className='p-2 bg-gray-50 col-span-1 row-span-4 border border-gray-300 shadow-md rounded-3xl'>List hadir</div>
        {_renderWeddingDetail()}
        <div className='p-2 bg-gray-50 col-span-1 row-span-4 border border-gray-300 shadow-md rounded-3xl normal-case w-full flex flex-col justify-center items-center h-full'>
          <Chat chatboxStyle={'flex flex-col h-full  bg-gray-50 md:w-full text-gray-700 p-2 rounded-2xl'} />
          <p className='text-sm text-gray-500'>Guna nama "Amer 5391" atau "Mariah 5391" untuk reply sebagai pengantin</p>
        </div>
      </div>
      <EditDate toggle={toggleEditDate} setToggle={setToggleEditDate} parentRefresh={fetchWedding} />
      <EditInfo toggle={toggleEditInfo} setToggle={setToggleEditInfo} parentRefresh={fetchWedding} weddingInfo={weddingInfo} />
      <EditAddress toggle={toggleEditAddress} setToggle={setToggleEditAddress} parentRefresh={fetchWedding} locationInfo={weddingInfo.location} />

    </>
  )
}
