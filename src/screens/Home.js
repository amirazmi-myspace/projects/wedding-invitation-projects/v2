import React, { useEffect, useState } from 'react'
import { DATABASE } from '../configs/firebase'
import { HomeContact } from './HomeContact'
import { HomeGreeting } from './HomeGreeting'
import { HomeHero } from './HomeHero'

export const Home = () => {

  const [wedding, setWedding] = useState({})
  const [load, setLoad] = useState(1)

  useEffect(() => {
    fetchWedding()
  }, [])

  const fetchWedding = async () => {
    const who = 'bride'
    await DATABASE.ref(`wedding/${who}`).on('value', (snapshot) => {
      if (snapshot.exists()) setWedding(snapshot.val())
      setLoad(2)
    })
  }


  if (load === 1) return (
    <div className='container w-full'>
      <p className='text-6xl text-center'>Loading...</p>
    </div>
  )
  if (load === 2) {
    return (
      <div className='w-full overflow-x-hidden'>
        <HomeHero weddingInfo={wedding} />
        <HomeGreeting weddingInfo={wedding} />
        <HomeContact weddingInfo={wedding} />
      </div>
    )
  }
}
